#!/bin/bash

# **Important** Both TFC_ORG and TFC_TOKEN environment variables should be set prior to running this script
# This script uploads a Sentinel policy to Terraform Cloud using the Policies API
# We assume the workspace name is my-gcp-gitlab-pipeline, please adjust this if needed
# We assume the TFC hostname name is app.terraform.io, please adjust this if needed
export TFC_ADDR="app.terraform.io"
if [ -z $TFC_WORKSPACE ]; then
  # Override default workspace name if needed
  export TFC_WORKSPACE="my-gcp-gitlab-pipeline"
  echo "Using default Workspace name: $TFC_WORKSPACE"
fi

# Validate that variables are set and obtain the workspace ID
./check_tfc_vars.sh
workspace_id=$(cat workspace_id)
# Check exit code
if [ -z "$workspace_id" ]
then
  echo "Could not get workspace ID, exitting."
  exit 
fi

# Write payload
cat <<EOF >payload.json
{
  "data": {
    "attributes": {
      "is-destroy":true,
      "message": "Destroy workspace using API"
    },
    "type":"runs",
    "relationships": {
      "workspace": {
        "data": {
          "type": "workspaces", "id": "$workspace_id"
        }
      }
    }
  }
}
EOF

echo "Queueing a destroy Run"
curl --header "Authorization: Bearer ${TFC_TOKEN}" \
  --header "Content-Type: application/vnd.api+json" \
  --request POST \
  --data @payload.json \
  https://${TFC_ADDR}/api/v2/runs > destroy_run.json

run_id=$(cat destroy_run.json | jq -r .data.id)
echo "Queued Response Run ID: ${run_id}"

# Evaluate $run_id and display URL
if [ ! -z "$run_id" ]; then
  url="https://${TFC_ADDR}/app/${TFC_ORG}/workspaces/${TFC_WORKSPACE}/runs/${run_id}"
  echo "A Destroy Run was queued successfully. Pausing 120 seconds for Plan to complete before applying Destroy Run"
  echo "Please visit the following URL to see the Run"
  echo "$url"
  sleep 120
  echo "Applying Run"
  curl \
  --header "Authorization: Bearer $TFC_TOKEN" \
  --header "Content-Type: application/vnd.api+json" \
  --request POST \
  https://${TFC_ADDR}/api/v2/runs/${run_id}/actions/apply
  echo "**Important**: Please visit the following URL to ensure destroy completed successfully."
  echo "$url"

else
  echo "Did not get a successful Run ID. Please retry or queue a Destroy Run from the TFC UI below"
  echo "https://${TFC_ADDR}/app/${TFC_ORG}/workspaces/${TFC_WORKSPACE}/runs"
  exit
fi


